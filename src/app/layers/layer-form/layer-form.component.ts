import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { faCogs, faCaretDown, faCaretUp } from '@fortawesome/free-solid-svg-icons';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { AngularFirestoreDocument } from '@angular/fire/firestore';

import { MapBoxComponent } from '../../shared/widgets/map-box/map-box.component';

import { FirebaseService } from '../../shared/services/firebase.service';
import { SlugifyService } from '../../shared/services/slugify.service';
import { NotifyService } from '../../shared/services/notify.service';

@Component({
  selector: 'app-layer-form',
  templateUrl: './layer-form.component.html',
  styleUrls: ['./layer-form.component.scss']
})
export class LayerFormComponent implements OnInit, OnDestroy {
  @ViewChild( MapBoxComponent ) mapComponentInstance: MapBoxComponent;
  debug = false;

  defaultColor = 'rgba(86, 38, 125, 1)';
  defaultStrokeColor = 'rgba(86, 38, 125, 1)';
  defaultStrokeWidth = 1;
  defaultFillColor = 'rgba(86, 38, 125, .75)';
  dashArray = [3, 3];

  layerDoc: AngularFirestoreDocument<any>;
  layer: Observable<any>;
  layerSub: Subscription;
  mapViews: Observable<any>;
  mapViewsSub: Subscription;
  periods: Observable<any>;
  periodsSub: Subscription;
  periodsObj: any;
  isNewItem = true;
  itemSingular = 'livello';
  itemPlural = 'livelli';
  showSpinner = true;

  layerForm: FormGroup;
  formErrors = {
    name: '',
    slug: '',
    startDate: '',
    endDate: '',
    description: ''
  };
  validationMessages = {
    startDate: {
      required: 'Questo campo è obbligatorio.'
    },
    endDate: {
      required: 'Questo campo è obbligatorio.'
    },
    name: {
      required: 'Questo campo è obbligatorio.'
    },
    slug: {
      required: 'Questo campo è obbligatorio.'
    }
  };

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    placeholder: `Una breve descrizione testuale del ${this.itemSingular} (facoltativo)...`,
    translate: 'no'
  };

  faCogs = faCogs;
  faCaretDown = faCaretDown;
  faCaretUp = faCaretUp;

  activePeriodID: string;
  layerPropertiesCollapsed = true;
  copyGeometryCollapsed = true;

  layerProperties = null;

  fakePatterns = [
    { id: 0, name: 'Pattern-1', color: this.defaultFillColor },
    { id: 1, name: 'Pattern-2', color: this.defaultFillColor },
    { id: 2, name: 'Pattern-3', color: this.defaultFillColor },
    { id: 3, name: 'Pattern-4', color: this.defaultFillColor },
    { id: 4, name: 'Pattern-5', color: this.defaultFillColor },
    { id: 5, name: 'Pattern-6', color: this.defaultFillColor },
    { id: 6, name: 'Pattern-7', color: this.defaultFillColor },
    { id: 7, name: 'Pattern-8', color: this.defaultFillColor },
    { id: 8, name: 'Pattern-9', color: this.defaultFillColor },
    { id: 9, name: 'Pattern-10', color: this.defaultFillColor }
  ];

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private firebase: FirebaseService,
    private slugifySvc: SlugifyService,
    private notify: NotifyService
  ) { }

  ngOnInit() {
    this.layerForm = this.fb.group({
      name: [ '', Validators.required ],
      slug: [ '', Validators.required ],
      order: 0,
      created: '',
      lastUpdated: '',
      // geometry: null,
      geometries: null,
    });

    this.mapViews = this.firebase.getCollection('/map-views');
    this.periods = this.firebase.getOrderedCollection('/periods', 'startDate', 1);
    this.periodsSub = this.periods.subscribe((data) => {
      this.periodsObj = data;
      this.activePeriodID = data[0].id;
      this.layerProperties = {};
      data.map((period: any) => {
        this.layerProperties[period.id] = {
          type: 'POINTS',
          description: '',
          strokeColor: this.defaultStrokeColor,
          strokeWidth: this.defaultStrokeWidth,
          fillColor: this.defaultFillColor,
          hasAreaStroke: false,
          hasDashedStroke: false,
          dashPattern: {
            strokeWidth: 4,
            spacing: 2
          },
          hasPatternFill: false,
          patternFill: null
        };
      });
    });

    this.layerForm.valueChanges.subscribe((data) => this.onValueChanged(data));
    this.onValueChanged();

    this.route.params.subscribe(
      (routeParams) => {
        if (routeParams.id) {
          this.isNewItem = false;

          this.layerDoc = this.firebase.getDocument('/layers/' + routeParams.id);
          this.layer = this.layerDoc.snapshotChanges().pipe(
            map(item => {
              const data = item.payload.data();
              const id = item.payload.id;
              return { id, ...data };
            })
          );
          this.layerSub = this.layer.subscribe(
            (data) => {
              this.layerForm.setValue({
                name: data.name ? data.name : null,
                slug: data.slug ? data.slug : null,
                order: data.order ? data.order : 0,
                created: data.created ? data.created : Date.now(),
                lastUpdated: data.lastUpdated ? data.lastUpdated : Date.now(),
                // geometry: data.geometry ? data.geometry : null,
                geometries: data.geometries ? data.geometries : null,
              });

              this.layerProperties = data.properties;

              if (this.mapComponentInstance) {
                if (!this.layerProperties) {
                  this.layerProperties = {};
                  this.periodsObj.map((period: any) => {
                    this.layerProperties[period.id] = {
                      type: 'POINTS',
                      description: '',
                      strokeColor: this.defaultStrokeColor,
                      strokeWidth: this.defaultStrokeWidth,
                      fillColor: this.defaultFillColor,
                      hasAreaStroke: false,
                      hasDashedStroke: false,
                      dashPattern: {
                        strokeWidth: 4,
                        spacing: 2
                      },
                      hasPatternFill: false,
                      patternFill: null
                    };
                  });
                }

                this.mapComponentInstance.drawMode = this.layerProperties[this.activePeriodID].type || 'POINTS';
                this.mapComponentInstance.initMap( this.layerForm.get('geometries').value, this.activePeriodID );
              }

              this.showSpinner = false;
            },
            (err) => this.notify.update('Error!', err, 'error'),
            () => console.log('complete')
          );
        } else {
          this.layerForm.patchValue({ type: 'POINTS' });
          this.mapComponentInstance.drawMode = 'POINTS';
          this.showSpinner = false;
        }
      },
      (err) => this.notify.update('Error!', err, 'error'),
      () => console.log('complete')
    );
  }

  ngOnDestroy() {
    if (this.layerSub) { this.layerSub.unsubscribe(); }
    if (this.mapViewsSub) { this.mapViewsSub.unsubscribe(); }
    if (this.periodsSub) { this.periodsSub.unsubscribe(); }
  }

  onValueChanged(data?: any) {
    if (!this.layerForm) { return; }

    const form = this.layerForm;

    if (data) {
      form.get('slug').patchValue( this.slugifySvc.slugify( data.name ), { emitEvent: false } );
    }

    for (const field in this.formErrors) {
      if ( Object.prototype.hasOwnProperty.call( this.formErrors, field ) ) {
        this.formErrors[field] = '';

        const control = form.get(field);

        if (control && !control.valid) {
          const messages = this.validationMessages[field];
          if (control.errors) {
            for (const key in control.errors) {
              if (Object.prototype.hasOwnProperty.call(control.errors, key) ) {
                this.formErrors[field] += `${(messages as {[key: string]: string})[key]} `;
              }
            }
          }
        }
      }
    }
  }

  onLayerTypeChange(e: any) {
    this.mapComponentInstance.drawMode = e;
    this.mapComponentInstance.initMap( this.layerForm.get('geometries').value, this.activePeriodID );
    this.layerProperties[this.activePeriodID].type = e;
    this.layerForm.patchValue({ type: this.mapComponentInstance.drawMode });
  }

  switchPeriod(e: MouseEvent, periodID: string) {
    e.preventDefault();
    this.copyGeometryCollapsed = true;
    this.activePeriodID = periodID;
    this.mapComponentInstance.drawMode = this.layerProperties[this.activePeriodID].type;
    this.mapComponentInstance.initMap( this.layerForm.get('geometries').value, this.activePeriodID );
  }

  toggleLayerProperties(e: MouseEvent) {
    e.preventDefault();
    this.layerPropertiesCollapsed = !this.layerPropertiesCollapsed;
  }

  toggleCopyGeometryDropdown(e: MouseEvent) {
    e.preventDefault();
    this.copyGeometryCollapsed = !this.copyGeometryCollapsed;
  }

  copyGeometry(e: MouseEvent, periodIDFrom: string) {
    e.preventDefault();
    const savedGeometries = this.layerForm.get('geometries').value;
    savedGeometries[this.activePeriodID] = savedGeometries[periodIDFrom];
    this.layerForm.patchValue({geometries: savedGeometries});
    this.mapComponentInstance.initMap( this.layerForm.get('geometries').value, this.activePeriodID );

    this.copyGeometryCollapsed = true;
  }

  onMapLoad() {
    console.log('map loaded');
  }

  save() {
    this.showSpinner = true;

    this.layerForm.patchValue({ lastUpdated: Date.now() });
    // this.layerForm.patchValue({ properties: this.layerProperties });

    const layerObj = this.layerForm.getRawValue();
    layerObj.properties = this.layerProperties;

    if ( this.isNewItem ) {
      layerObj.created = Date.now();

      this.firebase.createDocument( `layers`,  layerObj ).then(
        (res) => {
          this.mapViewsSub = this.mapViews.subscribe(
            (mapViews) => {
              if (mapViews && mapViews.length > 0) {
                const promisesArray = [];
                for (const mapView of mapViews) {
                  mapView.layers[res.id] = { enabled: true };
                  promisesArray.push( this.firebase.updateDocument('/map-views/' + mapView.id, mapView) );
                }

                const promise = Promise.all(promisesArray);
                promise.then(
                  () => {
                    this.showSpinner = false;
                    this.notify.update('Great!', 'Item created', 'success');
                    this.router.navigate(['/layers', 'edit', res.id]);
                  }
                );
              } else {
                this.showSpinner = false;
                this.notify.update('Great!', 'Item created', 'success');
                this.router.navigate(['/layers', 'edit', res.id]);
              }
            }
          );
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    } else {
      layerObj.geometries = this.mapComponentInstance.getGeometry();

      this.layerDoc.update( layerObj ).then(
        () => {
          this.showSpinner = false;
          this.notify.update('Great!', 'Item updated', 'success');
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    }
  }
}

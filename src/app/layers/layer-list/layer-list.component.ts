import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { faPlus, faCheckCircle, faBan } from '@fortawesome/free-solid-svg-icons';

import { NotifyService } from '../../shared/services/notify.service';
import { FirebaseService } from '../../shared/services/firebase.service';

@Component({
  selector: 'app-layer-list',
  templateUrl: './layer-list.component.html',
  styleUrls: ['./layer-list.component.scss']
})
export class LayerListComponent implements OnInit, OnDestroy {

  layers: Observable<any>;
  layersSub: Subscription;
  mapViews: Observable<any>;
  mapViewsSub: Subscription;
  showSpinner = true;
  itemToDelete: number;

  faPlus = faPlus;
  faCheckCircle = faCheckCircle;
  faBan = faBan;

  constructor(
    private firebase: FirebaseService,
    private notify: NotifyService
  ) {}

  ngOnInit() {
    this.mapViews = this.firebase.getCollection('/map-views');
    this.layers = this.firebase.getOrderedCollection('/layers', 'order', 1);
    this.layersSub = this.layers.subscribe(
      () => {
        this.showSpinner = false;
      },
      (err) => console.log(err),
      () => console.log('complete')
    );
  }

  ngOnDestroy() {
    if (this.layersSub) { this.layersSub.unsubscribe(); }
    if (this.mapViewsSub) { this.mapViewsSub.unsubscribe(); }
  }

  deleteItem(e: MouseEvent, id: string, index: number) {
    e.preventDefault();

    this.showSpinner = true;
    this.itemToDelete = index;

    this.mapViewsSub = this.mapViews.subscribe(
      (mapViews: any) => {
        mapViews.map(
          (mapView: any) => {
            const updatedLayers = {};
            for (const j in mapView.layers) {
              if (j && j !== id) {
                updatedLayers[j] = mapView.layers[j];
              }
            }

            this.firebase.updateDocument('/map-views/' + mapView.id, { ...mapView, layers: updatedLayers }).then(
              () => {
                this.firebase.deleteDocument('layers', id).then(
                  () => {
                    this.showSpinner = false;
                    this.notify.update('Great!', 'Item deleted', 'success');
                  },
                  (err) => console.log( err )
                );
              },
              (err) => console.log( err )
            );
          }
        );
      }
    );
  }

}

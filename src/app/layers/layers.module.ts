import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CloudinaryModule } from '@cloudinary/angular-5.x';
import { FileUploadModule } from 'ng2-file-upload';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { DpDatePickerModule } from 'ng2-date-picker';

import { WidgetsModule } from '../shared/widgets/widgets.module';

import { LayerListComponent } from './layer-list/layer-list.component';
import { LayerFormComponent } from './layer-form/layer-form.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    CloudinaryModule,
    FileUploadModule,
    AngularEditorModule,
    DpDatePickerModule,
    WidgetsModule
  ],
  declarations: [
    LayerListComponent,
    LayerFormComponent
  ]
})
export class LayerssModule {
}

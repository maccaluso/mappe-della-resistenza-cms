export interface Geolocation {
	lng: number;
	lat: number;
}
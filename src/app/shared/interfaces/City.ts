import { Geolocation } from './Geolocation';

export interface City {
	$key: string;
	name: string;
	slug: string;
	type: string;
	geolocation: Geolocation;
	description: string;
	images: any[];
	videos: any[];	
	active:boolean;
	created: any;
	lastUpdated: any;
	author: any;
}
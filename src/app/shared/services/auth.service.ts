import { Injectable } from '@angular/core';

import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { NotifyService } from './notify.service';

import { User } from '../interfaces/User';

import { Observable } from 'rxjs';

@Injectable()
export class AuthService {

    user: Observable<User>;

    constructor(
        private afAuth: AngularFireAuth,
        private notify: NotifyService
    ) {
        this.user = this.afAuth.authState;
    }

    emailLogin(email: string, password: string) {
        return this.afAuth.auth.signInWithEmailAndPassword(email, password);
    }

    resetPassword(email: string) {
        const fbAuth = firebase.auth();

        return fbAuth.sendPasswordResetEmail(email);
    }

    signOut() {
        return this.afAuth.auth.signOut();
    }

    private handleError(error: Error) {
        console.error(error);
        this.notify.update('Something went wrong!', error.message, 'error');
    }
}

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-symbol',
  templateUrl: './symbol.component.html',
  styleUrls: ['./symbol.component.scss']
})
export class SymbolComponent implements OnInit {
  @Input() type: string;
  @Input() fillColor: string;
  @Input() fillOpacity: number;
  @Input() strokeColor: string;
  @Input() strokeOpacity: number;
  @Input() strokeWidth: number;

  constructor() { }

  ngOnInit() {}

}

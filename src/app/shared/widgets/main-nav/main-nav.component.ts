import { Component, OnInit } from '@angular/core';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss']
})
export class MainNavComponent implements OnInit {

  show = false;

  constructor(public auth: AuthService) { }

  ngOnInit() {}

  toggleCollapse() {
    this.show = !this.show;
  }

  logOut() {
    this.auth.signOut().then(
      () => { console.log( 'Logged out' ); },
      (err) => { console.log( err ); }
    );
  }

}

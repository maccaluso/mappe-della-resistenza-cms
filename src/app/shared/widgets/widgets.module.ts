import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CloudinaryModule } from '@cloudinary/angular-5.x';
import { FileUploadModule } from 'ng2-file-upload';
import { ColorPickerModule } from 'ngx-color-picker';

import { FooterComponent } from './footer/footer.component';
import { FormUploaderComponent } from './form-uploader/form-uploader.component';
import { FormUploaderButtonComponent } from './form-uploader-button/form-uploader-button.component';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { MapBoxComponent } from './map-box/map-box.component';
import { NotificationMessageComponent } from './notification-message/notification-message.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { UserFormComponent } from './user-form/user-form.component';
import { SymbolComponent } from './symbol/symbol.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    CloudinaryModule,
    FileUploadModule,
    ColorPickerModule
  ],
  declarations: [
    FooterComponent,
    FormUploaderComponent,
    FormUploaderButtonComponent,
    LoadingSpinnerComponent,
    MainNavComponent,
    MapBoxComponent,
    NotificationMessageComponent,
    SidebarComponent,
    UserFormComponent,
    SymbolComponent
  ],
  exports: [
    FooterComponent,
    FormUploaderComponent,
    FormUploaderButtonComponent,
    LoadingSpinnerComponent,
    MainNavComponent,
    MapBoxComponent,
    NotificationMessageComponent,
    SidebarComponent,
    UserFormComponent,
    ColorPickerModule,
    SymbolComponent
  ]
})
export class WidgetsModule { }
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { faEnvelope, faCheck, faLock, faBan } from '@fortawesome/free-solid-svg-icons';

import { AuthService } from '../../services/auth.service';
import { NotifyService } from '../../services/notify.service';

type UserFields = 'email' | 'password';
type FormErrors = { [u in UserFields]: string };

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
})
export class UserFormComponent implements OnInit, OnDestroy {

  userForm: FormGroup;
  newUser = false;
  passReset = false;
  formErrors: FormErrors = {
    'email': 'Email is required and must be a valid email address.',
    'password': 'Password is required and must be between 6 and 40 characters long.',
  };
  validationMessages = {
    'email': {
      'required': 'Email is required.',
      'email': 'Email must be a valid email',
    },
    'password': {
      'required': 'Password is required.',
      // 'pattern': 'Password must be include at least one letter and one number.',
      'minlength': 'Password must be at least 6 characters long.',
      'maxlength': 'Password cannot be more than 40 characters long.',
    },
  };

  faEnvelope = faEnvelope;
  faCheck = faCheck;
  faLock = faLock;
  faBan = faBan;

  private formChangesSub: any;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private notify: NotifyService
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  ngOnDestroy() {
    this.formChangesSub.unsubscribe();
  }

  // signup() {
  //   this.auth.emailSignUp(this.userForm.value['email'], this.userForm.value['password']);
  //   console.log('signup disabled');
  // }

  login() {
    this.auth.emailLogin( this.userForm.value['email'], this.userForm.value['password'] ).then(
      (user) => this.notify.update('Great!', 'You\'ve successfully logged in', 'success'),
      (err) => this.notify.update('Ops!', 'Something went wrong :-(', 'error')
    );
  }

  resetPassword() {
    this.auth.resetPassword( this.userForm.value['email'] ).then(
      () => {
        console.log( 'Password reset email sent' );
        this.passReset = true;
      },
      (err) => console.log( err )
    );
  }

  buildForm() {
    this.userForm = this.fb.group({
      'email': ['', [
        Validators.required,
        Validators.email,
      ]],
      'password': ['', [
        // Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
        Validators.minLength(6),
        Validators.maxLength(25),
      ]],
    });

    this.formChangesSub = this.userForm.valueChanges.subscribe((data) => this.onValueChanged(data));
    this.onValueChanged(); // reset validation messages
  }

  // Updates validation state on form changes.
  onValueChanged(data?: any) {
    if (!this.userForm) { return; }
    const form = this.userForm;
    for (const field in this.formErrors) {
      if (Object.prototype.hasOwnProperty.call(this.formErrors, field) && (field === 'email' || field === 'password')) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          if (control.errors) {
            for (const key in control.errors) {
              if (Object.prototype.hasOwnProperty.call(control.errors, key) ) {
                this.formErrors[field] += `${(messages as {[key: string]: string})[key]} `;
              }
            }
          }
        }
      }
    }
  }
}

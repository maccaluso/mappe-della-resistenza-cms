import { Component, Input, Output, EventEmitter, NgZone } from '@angular/core';
import { first } from 'rxjs/operators';

import * as mapboxgl from 'mapbox-gl';

import { environment } from '../../../../environments/environment';

@Component({
  selector: 'map-box',
  templateUrl: './map-box.component.html',
  styleUrls: ['./map-box.component.scss']
})
export class MapBoxComponent {
  @Input() styleUrl = environment.mapbox.styleUrl;
  @Input() zoom: number;
  @Input() containerH = '350px';
  @Input() markerW = '15px';
  @Input() markerH = '15px';
  @Input() markerColor = 'rgba(86, 38, 125, 1)';
  @Input() markerTextColor = '#ffffff';
  @Input() markerFontSize = '10px';
  @Input() drawMode = 'POINTS';
  @Input() lockToSinglePointMode = false;
  @Input() enabled = true;
  @Output() mapLoaded = new EventEmitter<boolean>();
  @Output() mapClicked = new EventEmitter<any>();
  @Output() markerDragEnded = new EventEmitter<any>();

  public defaultLocation: number[] = [9.172066045315233, 40.792836923962426];

  private mapInstance: mapboxgl.Map;
  private defaultZoom = 3.5;
  private cityLevelZoom = 10;

  private activePeriodID: string;

  private geoms: any = {};
  private currentPointIndex = 0;
  private currentGeomIndex = 0;
  private canEditPoint = true;

  private mapEventsCallbacks = [
    (moveEvt: mapboxgl.MapLayerMouseEvent) => {
      this.geoms[this.activePeriodID][this.currentGeomIndex][this.currentPointIndex] = [moveEvt.lngLat.lng, moveEvt.lngLat.lat];

      this.renderPoints();
      if (this.drawMode === 'LINES') { this.renderLines(); }
      if (this.drawMode === 'AREAS') { this.renderAreas(); }
    },
    (upEvt: mapboxgl.MapMouseEvent) => {
      if (this.drawMode === 'POINTS') { this.markerDragEnded.emit(upEvt.lngLat); }

      this.mapInstance.off('mousemove', this.mapEventsCallbacks[0]);
      this.mapInstance.off('touchmove', this.mapEventsCallbacks[0]);
    }
  ];

  constructor(private zone: NgZone) {
    this.zone.onStable.pipe(first()).subscribe(() => {
      this.assign(mapboxgl, 'accessToken', environment.mapbox.accessToken);
      this.createMap();
    });
  }

  public initMap(geometry: any, periodID: string) {
    this.clearMap();
    this.activePeriodID = periodID;
    this.initGeometry(geometry);
  }

  public jump(location: number[]) {
    this.mapInstance.jumpTo({
      center: (location as mapboxgl.LngLatLike),
      zoom: this.cityLevelZoom
    });
  }

  private createMap() {
    this.zone.runOutsideAngular(() => {
      this.mapInstance = new mapboxgl.Map({
        container: 'map',
        style: this.styleUrl,
        zoom: this.defaultZoom,
        center: this.defaultLocation as mapboxgl.LngLatLike
      });
      this.mapInstance.addControl(new mapboxgl.NavigationControl());

      this.mapInstance.getCanvasContainer().style.cursor = 'default';
      this.mapInstance.getContainer().style.height = this.containerH;
      this.mapInstance.resize();

      this.mapInstance.on('load', () => { this.mapLoaded.emit(true); });
      this.mapInstance.on('click', (evt: mapboxgl.MapMouseEvent) => { this.handleClick(evt); });

      this.mapInstance.on('mousedown', 'points', (evt: mapboxgl.MapMouseEvent) => {
        evt.preventDefault();

        this.mapInstance.on('mousemove', this.mapEventsCallbacks[0]);
        this.mapInstance.once('mouseup', this.mapEventsCallbacks[1]);
      });

      this.mapInstance.on('touchstart', 'points', (evt: mapboxgl.MapTouchEvent) => {
        evt.preventDefault();

        this.mapInstance.on('touchmove', this.mapEventsCallbacks[0]);
        this.mapInstance.once('touchend', this.mapEventsCallbacks[1]);
      });
    });
  }

  private clearMap() {
    if (this.mapInstance.getLayer('lines')) { this.mapInstance.removeLayer('lines'); }
    if (this.mapInstance.getLayer('areas')) { this.mapInstance.removeLayer('areas'); }
    if (this.mapInstance.getLayer('points')) { this.mapInstance.removeLayer('points'); }
    if (this.mapInstance.getLayer('pointNumbers')) { this.mapInstance.removeLayer('pointNumbers'); }
    if (this.mapInstance.getSource('lines')) { this.mapInstance.removeSource('lines'); }
    if (this.mapInstance.getSource('areas')) { this.mapInstance.removeSource('areas'); }
    if (this.mapInstance.getSource('points')) { this.mapInstance.removeSource('points'); }
    if (this.mapInstance.getSource('pointNumbers')) { this.mapInstance.removeSource('pointNumbers'); }

    this.geoms = {};
    this.currentGeomIndex = 0;
    this.currentPointIndex = 0;
  }

  private initGeometry(geometryData: any) {
    if (geometryData) {
      for (const period in geometryData) {
        if (period) {
          this.geoms[period] = [];
          for (const i in geometryData[period]) {
            if (i) {
              this.geoms[period][i] = [];

              for (const j in geometryData[period][i]) {
                if (j) {
                  this.geoms[period][i].push(geometryData[period][i][j]);
                }
              }
            }
          }
        }
      }

      if (this.geoms[this.activePeriodID]) {
        this.currentGeomIndex = this.geoms[this.activePeriodID].length - 1;
        this.currentPointIndex = this.geoms[this.activePeriodID][this.currentGeomIndex].length - 1;
      }
    }

    if ( !this.geoms[this.activePeriodID] ) {
      this.geoms[this.activePeriodID] = [];
      this.addLine();
      return;
    }

    this.renderPoints();

    if (this.drawMode === 'LINES') { this.renderLines(); }
    if (this.drawMode === 'AREAS') { this.renderAreas(); }
  }

  private handleClick(e: mapboxgl.MapMouseEvent) {
    e.preventDefault();

    const pointFeatures = this.mapInstance.queryRenderedFeatures(e.point).filter((feature) => {
      return feature.layer.id === 'points';
    });

    if (pointFeatures.length > 0) {
      for (const p of pointFeatures) {
        if (p.properties.geomIndex === this.currentGeomIndex && p.properties.pointIndex === this.currentPointIndex) {
          console.log('Ho cliccato il marker corrente');
          return;
        } else {
          this.currentGeomIndex = p.properties.geomIndex;
          this.currentPointIndex = p.properties.pointIndex;
          this.renderPoints();
          return;
        }
      }
    } else {
      if (this.drawMode === 'POINTS' && this.lockToSinglePointMode && this.geoms[this.activePeriodID][this.currentGeomIndex]) { return; }

      this.zone.run(() => {
        if (this.geoms[this.activePeriodID]) {
          if (this.geoms[this.activePeriodID][this.currentGeomIndex]) {
            this.geoms[this.activePeriodID][this.currentGeomIndex].push([e.lngLat.lng, e.lngLat.lat]);
          } else {
            this.geoms[this.activePeriodID][this.currentGeomIndex] = [[e.lngLat.lng, e.lngLat.lat]];
          }

          this.currentPointIndex++;
        } else {
          this.geoms[this.activePeriodID] = [];
          this.addLine();
          this.canEditPoint = true;
        }
      });

      this.mapClicked.emit({ coords: e.lngLat, lines: this.geoms });

      this.renderPoints();
      if (this.drawMode === 'LINES') { this.renderLines(); }
      if (this.drawMode === 'AREAS') { this.renderAreas(); }
    }
  }

  private renderPoints() {
    const pointsCollection = {
      type: 'FeatureCollection',
      features: []
    };

    this.geoms[this.activePeriodID].map(
      (geom: any, i: number) => {
        geom.map(
          (point: any, j: number) => {
            pointsCollection.features.push({
              type: 'Feature',
              geometry: {
                type: 'Point',
                coordinates: point
              },
              properties: {
                geomIndex: i,
                pointIndex: j,
                pointLabel: j + 1,
                selected: i === this.currentGeomIndex && j === this.currentPointIndex ? 'true' : 'false'
              }
            });
          }
        );
      }
    );

    if (this.mapInstance.getLayer('points')) {
      (this.mapInstance.getSource('points') as mapboxgl.GeoJSONSource)
        .setData((pointsCollection as GeoJSON.FeatureCollection));
      (this.mapInstance.getSource('pointNumbers') as mapboxgl.GeoJSONSource)
        .setData((pointsCollection as GeoJSON.FeatureCollection));
    } else {
      this.mapInstance.addLayer({
        id: 'points',
        type: 'circle',
        source: {
          type: 'geojson',
          data: (pointsCollection as GeoJSON.FeatureCollection)
        },
        paint: {
          'circle-radius': [
            'interpolate',
            ['linear'],
            ['zoom'],
            8, parseInt(this.markerW.replace('px', ''), 10) / 3.5,
            10, parseInt(this.markerW.replace('px', ''), 10) / 2.5,
            12, parseInt(this.markerW.replace('px', ''), 10) / 1.5,
            14, parseInt(this.markerW.replace('px', ''), 10),
            16, parseInt(this.markerW.replace('px', ''), 10) * 1.5
          ],
          'circle-color': this.markerColor,
          'circle-stroke-width': [
            'match',
            ['get', 'selected'],
            'true', 2, 0
          ],
          'circle-stroke-color': 'rgba(255, 0, 0, 1)'
        }
      });

      this.mapInstance.addLayer({
        id: 'pointNumbers',
        type: 'symbol',
        source: {
          type: 'geojson',
          data: (pointsCollection as GeoJSON.FeatureCollection)
        },
        layout: {
          'text-field': '{pointLabel}',
          'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
          'text-size': [
            'interpolate',
            ['linear'],
            ['zoom'],
            8, parseInt(this.markerW.replace('px', ''), 10) / 3.5,
            10, parseInt(this.markerW.replace('px', ''), 10) / 2.5,
            12, parseInt(this.markerW.replace('px', ''), 10) / 1.5,
            14, parseInt(this.markerW.replace('px', ''), 10),
            16, parseInt(this.markerW.replace('px', ''), 10) * 1.5
          ]
        },
        paint: {
          'text-color': this.markerTextColor
        }
      });
    }
  }

  private renderLines() {
    const lineCollection = {
      type: 'FeatureCollection',
      features: []
    };

    for (const line in this.geoms[this.activePeriodID]) {
      if (line) {
        const lineFeature = {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'LineString',
            coordinates: []
          }
        };

        for (const point in this.geoms[this.activePeriodID][line]) {
          if (point) {
            lineFeature.geometry.coordinates.push(this.geoms[this.activePeriodID][line][point]);
          }
        }

        lineCollection.features.push((lineFeature as GeoJSON.Feature));
      }
    }

    console.log(lineCollection);

    if (this.mapInstance.getLayer('lines')) {
      (this.mapInstance.getSource('lines') as mapboxgl.GeoJSONSource).setData((lineCollection as GeoJSON.FeatureCollection));
    } else {
      this.mapInstance.addLayer({
        id: 'lines',
        type: 'line',
        source: {
          type: 'geojson',
          data: (lineCollection as GeoJSON.FeatureCollection)
        },
        layout: {
          'line-join': 'round',
          'line-cap': 'round'
        },
        paint: {
          'line-color': this.markerColor,
          'line-width': 2
        }
      }, 'points');
    }
  }

  private renderAreas() {
    const polygonCollection = {
      type: 'FeatureCollection',
      features: []
    };

    for (const line in this.geoms[this.activePeriodID]) {
      if (line) {
        const polygonFeature = {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Polygon',
            coordinates: [[]]
          }
        };

        for (const point in this.geoms[this.activePeriodID][line]) {
          if (point) {
            polygonFeature.geometry.coordinates[0].push(this.geoms[this.activePeriodID][line][point]);
          }
        }

        polygonCollection.features.push((polygonFeature as GeoJSON.Feature));
      }
    }

    if (this.mapInstance.getLayer('areas')) {
      (this.mapInstance.getSource('areas') as mapboxgl.GeoJSONSource).setData((polygonCollection as GeoJSON.FeatureCollection));
    } else {
      // const patternW = 16;
      // const bytesPerPixel = 4; // Each pixel is represented by 4 bytes: red, green, blue, and alpha.
      // const imageArray = new Uint8Array(patternW * patternW * bytesPerPixel);

      // for (let x = 0; x < patternW; x++) {
      //   for (let y = 0; y < patternW; y++) {
      //     const offset = (y * patternW + x) * bytesPerPixel;
      //     imageArray[ offset + 0 ] = x % 8 ? 0 : 200;
      //     imageArray[ offset + 1 ] = x % 8 ? 0 : 123;
      //     imageArray[ offset + 2 ] = x % 8 ? 0 : 72;
      //     imageArray[ offset + 3 ] = x % 8 ? 0 : 255;
      //   }
      // }

      // const geoptrn = GeoPattern.generate('GitHub');
      // const base64 = geoptrn.toBase64();
      // const raw = window.atob(base64);
      // const rawLength = raw.length;
      // const array = new Uint8Array(new ArrayBuffer(rawLength));
      // for (let i = 0; i < rawLength; i++) { array[i] = raw.charCodeAt(i); }

      // // console.log(imageArray, array, rawLength);
      // console.log( rawLength, imageArray.length );

      // this.mapInstance.addImage('pattern', {width: patternW, height: patternW, data: imageArray});
      // this.mapInstance.addImage('pattern', {width: rawLength, height: rawLength, data: array});

      this.mapInstance.addLayer({
        id: 'areas',
        type: 'fill',
        source: {
          type: 'geojson',
          data: (polygonCollection as GeoJSON.FeatureCollection)
        },
        paint: {
          'fill-color': this.markerColor,
          'fill-opacity': 0.8,
          'fill-outline-color': 'rgba(0, 0, 0, 1)'
          // 'fill-pattern': 'pattern'
        }
      }, 'points');
    }
  }

  private addLine() {
    const mapCenter = [this.mapInstance.getCenter().lng, this.mapInstance.getCenter().lat];

    this.geoms[this.activePeriodID].push([]);
    this.currentGeomIndex = this.geoms[this.activePeriodID].length - 1;
    this.geoms[this.activePeriodID][this.currentGeomIndex].push(mapCenter);
    this.currentPointIndex = 0;

    this.renderPoints();
  }

  private deleteSelectedVertex() {
    this.geoms[this.activePeriodID][this.currentGeomIndex].splice(this.currentPointIndex, 1);

    this.currentPointIndex = this.geoms[this.activePeriodID][this.currentGeomIndex].length - 1;

    if (this.currentPointIndex < 0) {
      this.geoms[this.activePeriodID].splice(this.currentGeomIndex, 1);

      this.currentGeomIndex = this.geoms[this.activePeriodID].length - 1;

      if (this.currentGeomIndex < 0) {
        this.currentGeomIndex = 0;
        this.currentPointIndex = 0;
        this.clearMap();
        this.canEditPoint = false;
        return;
      } else {
        this.currentPointIndex = this.geoms[this.activePeriodID][this.currentGeomIndex].length - 1;
      }
    }

    this.renderPoints();

    if (this.drawMode === 'LINES') { this.renderLines(); }
    if (this.drawMode === 'AREAS') { this.renderAreas(); }
  }

  public getGeometry(): any {
    const geomsObj = {};

    for (const period in this.geoms) {
      if (period) {
        geomsObj[period] = {};
        this.geoms[period].map (
          (line: any, i: number) => {
            geomsObj[period][i] = {};
            line.map (
              (point: any, j: number) => {
                geomsObj[period][i][j] = point;
              }
            );
          }
        );
      }
    }

    return geomsObj;
  }

  private assign(obj: any, prop: any, value: any) {
    if (typeof prop === 'string') {
      prop = prop.split('.');
    }
    if (prop.length > 1) {
      const e = prop.shift();
      this.assign(obj[e] =
        Object.prototype.toString.call(obj[e]) === '[object Object]'
          ? obj[e]
          : {},
        prop,
        value);
    } else {
      obj[prop[0]] = value;
    }
  }
}

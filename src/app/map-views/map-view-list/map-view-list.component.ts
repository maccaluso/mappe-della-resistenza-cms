import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { faPlus, faCheckCircle, faBan } from '@fortawesome/free-solid-svg-icons';

import { NotifyService } from '../../shared/services/notify.service';
import { FirebaseService } from '../../shared/services/firebase.service';

@Component({
  selector: 'app-map-view-list',
  templateUrl: './map-view-list.component.html',
  styleUrls: ['./map-view-list.component.scss']
})
export class MapViewListComponent implements OnInit, OnDestroy {

  mapViews: Observable<any>;
  mapViewsSub: Subscription;
  mapViewsData: any;
  showSpinner = true;
  itemToDelete: number;
  numCategories: number;

  faPlus = faPlus;
  faCheckCircle = faCheckCircle;
  faBan = faBan;

  constructor(
    private firebase: FirebaseService,
    private notify: NotifyService
  ) {}

  ngOnInit() {
    this.mapViews = this.firebase.getCollection('/map-views');
    this.mapViewsSub = this.mapViews.subscribe(
      (data) => {
        this.mapViewsData = data;
        this.showSpinner = false;
      },
      (err) => console.log(err),
      () => console.log('complete')
    );
  }

  ngOnDestroy() {
    if (this.mapViewsSub) { this.mapViewsSub.unsubscribe(); }
  }

  getNumCategories(mapID: string): number {
    let numCategories = 0;

    this.mapViewsData.map(
      (mapView: any) => {
        if (mapID === mapView.id) {
          for (const catID in mapView.categories) {
            if (catID) {
              if ( mapView.categories[catID].enabled ) { numCategories++; }
            }
          }
        }
      }
    );

    return numCategories;
  }

  deleteItem(e: MouseEvent, id: string, index: number) {
    e.preventDefault();

    this.showSpinner = true;
    this.itemToDelete = index;

    this.firebase.deleteDocument('map-views', id).then(
      () => {
        this.showSpinner = false;
        this.notify.update('Great!', 'Item deleted', 'success');
      },
      (err) => console.log( err )
    );
  }

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';

import { faBan, faCheck, faPlus, faUpload } from '@fortawesome/free-solid-svg-icons';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { AngularFirestoreDocument } from '@angular/fire/firestore';

import { FirebaseService } from '../../shared/services/firebase.service';
import { SlugifyService } from '../../shared/services/slugify.service';
import { NotifyService } from '../../shared/services/notify.service';

@Component({
  selector: 'app-map-view-form',
  templateUrl: './map-view-form.component.html',
  styleUrls: ['./map-view-form.component.scss']
})
export class MapViewFormComponent implements OnInit, OnDestroy {
  debug = false;

  mapViewDoc: AngularFirestoreDocument<any>
  mapView: Observable<any>;
  mapViewSub: Subscription;
  cities: Observable<any>;
  citiesSub: Subscription;
  citiesData: any;
  layers: Observable<any>;
  layersSub: Subscription;
  layersData: any;
  categories: Observable<any>;
  categoriesSub: Subscription;
  categoriesData: any;
  defaultCategories: any;
  isNewItem = true;
  itemSingular = 'mappa';
  itemPlural = 'mappe';
  showSpinner = true;

  mapViewForm: FormGroup;
  formErrors = {
    name: '',
    slug: '',
    startDate: '',
    endDate: '',
    description: ''
  };
  validationMessages = {
    startDate: {
      required: 'Questo campo è obbligatorio.'
    },
    endDate: {
      required: 'Questo campo è obbligatorio.'
    },
    name: {
      required: 'Questo campo è obbligatorio.'
    },
    slug: {
      required: 'Questo campo è obbligatorio.'
    }
  };

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    placeholder: `Una breve descrizione testuale della ${this.itemSingular} (facoltativo)...`,
    translate: 'no'
  };

  faCheck = faCheck;
  faBan = faBan;
  faPlus = faPlus;
  faUpload = faUpload;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private firebase: FirebaseService,
    private slugifySvc: SlugifyService,
    private notify: NotifyService
  ) { }

  ngOnInit() {
    this.cities = this.firebase.getCollection('/cities');
    const defaultCities = {};
    this.layers = this.firebase.getCollection('/layers');
    const defaultLayers = {};
    this.categories = this.firebase.getOrderedCollection('/categories', 'order', 1);
    this.defaultCategories = {};

    this.citiesSub = this.cities.subscribe(
      (cities) => {
        cities.map(
          (city: any) => {
            defaultCities[city.id] = {
              enabled: true
            }
          }
        );

        if (this.isNewItem) { this.citiesData = defaultCities; }
      },
      (err) => this.notify.update('Error!', err, 'error'),
      () => console.log('complete')
    );

    this.layersSub = this.layers.subscribe(
      (layers) => {
        layers.map(
          (city: any) => {
            defaultLayers[city.id] = {
              enabled: true
            };
          }
        );

        if (this.isNewItem) { this.layersData = defaultLayers; }
      },
      (err) => this.notify.update('Error!', err, 'error'),
      () => console.log('complete')
    );

    this.categoriesSub = this.categories.subscribe(
      (categories) => {
        categories.map(
          (category: any) => {
            this.defaultCategories[category.id] = {
              enabled: true
            };
          }
        );

        if (this.isNewItem) { this.categoriesData = this.defaultCategories; }
      },
      (err) => this.notify.update('Error!', err, 'error'),
      () => console.log('complete')
    );

    this.mapViewForm = this.fb.group({
      name: [ '', Validators.required ],
      slug: [ '', Validators.required ],
      type: [ '', Validators.required ],
      description: '',
      created: '',
      lastUpdated: ''
    });

    this.mapViewForm.valueChanges.subscribe((data) => this.onValueChanged(data));
    this.onValueChanged();

    this.route.params.subscribe(
      (routeParams) => {
        if (routeParams.id) {
          this.isNewItem = false;

          this.mapViewDoc = this.firebase.getDocument('/map-views/' + routeParams.id);
          this.mapView = this.mapViewDoc.valueChanges();
          this.mapViewSub = this.mapView.subscribe(
            (data) => {
              this.mapViewForm.setValue({
                name: data.name ? data.name : null,
                slug: data.slug ? data.slug : null,
                type: data.type ? data.type : null,
                description: data.description ? data.description : null,
                created: data.created ? data.created : Date.now(),
                lastUpdated: data.lastUpdated ? data.lastUpdated : Date.now()
              });

              this.citiesData = data.cities ? data.cities : null;
              this.layersData = data.layers ? data.layers : null;
              this.categoriesData = data.categories ? data.categories : this.defaultCategories;

              this.showSpinner = false;
            },
            (err) => this.notify.update('Error!', err, 'error'),
            () => console.log('complete')
          );
        } else {
          this.showSpinner = false;
        }
      },
      (err) => this.notify.update('Error!', err, 'error'),
      () => console.log('complete')
    );
  }

  ngOnDestroy() {
    if (this.mapViewSub) { this.mapViewSub.unsubscribe(); }
    if (this.citiesSub) { this.citiesSub.unsubscribe(); }
    if (this.layersSub) { this.layersSub.unsubscribe(); }
    if (this.categoriesSub) { this.categoriesSub.unsubscribe(); }
  }

  onValueChanged(data?: any) {
    if (!this.mapViewForm) { return; }

    const form = this.mapViewForm;

    if (data) {
      form.get('slug').patchValue( this.slugifySvc.slugify( data.name ), { emitEvent: false } );
    }

    for (const field in this.formErrors) {
      if ( Object.prototype.hasOwnProperty.call( this.formErrors, field ) ) {
        // clear previous error message (if any)
        this.formErrors[field] = '';

        const control = form.get(field);

        if (control && !control.valid) {
          const messages = this.validationMessages[field];
          if (control.errors) {
            for (const key in control.errors) {
              if (Object.prototype.hasOwnProperty.call(control.errors, key) ) {
                this.formErrors[field] += `${(messages as {[key: string]: string})[key]} `;
              }
            }
          }
        }
      }
    }
  }

  onCitySwitchChange(cityID: string) {
    if (this.citiesData[cityID]) {
      this.citiesData[cityID].enabled = !this.citiesData[cityID].enabled;
    } else {
      this.citiesData[cityID] = { enabled: true };
    }
  }

  onLayerSwitchChange(layer: string) {
    if (this.layersData[layer]) {
      this.layersData[layer].enabled = !this.layersData[layer].enabled;
    } else {
      this.layersData[layer] = { enabled: true };
    }
  }

  onCategorySwitchChange(category: string) {
    if (this.categoriesData[category]) {
      this.categoriesData[category].enabled = !this.categoriesData[category].enabled;
    } else {
      this.categoriesData[category] = { enabled: true };
    }
  }

  save() {
    this.showSpinner = true;

    this.mapViewForm.patchValue({ lastUpdated: Date.now() });

    const mapViewObj = {
      ...this.mapViewForm.getRawValue(),
      cities: this.citiesData,
      layers: this.layersData,
      categories: this.categoriesData
    };

    if ( this.isNewItem ) {
      mapViewObj.created = Date.now();

      this.firebase.createDocument( `map-views`,  mapViewObj ).then(
        (res) => {
          this.showSpinner = false;
          this.notify.update('Great!', 'Item created', 'success');
          this.router.navigate(['/map-views', 'edit', res.id]);
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    } else {
      this.mapViewDoc.update( mapViewObj ).then(
        () => {
          this.showSpinner = false;
          this.notify.update('Great!', 'Item updated', 'success');
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    }
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './shared/services/auth.guard';

import { FrontPageComponent } from './front-page/front-page.component';
import { UserLoginComponent } from './user-login/user-login.component';

import { PeriodListComponent } from './periods/period-list/period-list.component';
import { PeriodFormComponent } from './periods/period-form/period-form.component';

import { CategoryListComponent } from './categories/category-list/category-list.component';
import { CategoryFormComponent } from './categories/category-form/category-form.component';

import { CityListComponent } from './cities/city-list/city-list.component';
import { CityFormComponent } from './cities/city-form/city-form.component';

import { MapViewListComponent } from './map-views/map-view-list/map-view-list.component';
import { MapViewFormComponent } from './map-views/map-view-form/map-view-form.component';

import { LayerListComponent } from './layers/layer-list/layer-list.component';
import { LayerFormComponent } from './layers/layer-form/layer-form.component';

const routes: Routes = [
  { path: '', component: FrontPageComponent },
  { path: 'login', component: UserLoginComponent },

  { path: 'periods', component: PeriodListComponent, canActivate: [AuthGuard] },
  { path: 'periods/new', component: PeriodFormComponent, canActivate: [AuthGuard] },
  { path: 'periods/edit/:id', component: PeriodFormComponent, canActivate: [AuthGuard] },

  { path: 'categories', component: CategoryListComponent, canActivate: [AuthGuard] },
  { path: 'categories/new', component: CategoryFormComponent, canActivate: [AuthGuard] },
  { path: 'categories/edit/:id', component: CategoryFormComponent, canActivate: [AuthGuard] },

  { path: 'cities', component: CityListComponent, canActivate: [AuthGuard] },
  { path: 'cities/new', component: CityFormComponent, canActivate: [AuthGuard] },
  { path: 'cities/edit/:id', component: CityFormComponent, canActivate: [AuthGuard] },

  { path: 'map-views', component: MapViewListComponent, canActivate: [AuthGuard] },
  { path: 'map-views/new', component: MapViewFormComponent, canActivate: [AuthGuard] },
  { path: 'map-views/edit/:id', component: MapViewFormComponent, canActivate: [AuthGuard] },

  { path: 'layers', component: LayerListComponent, canActivate: [AuthGuard] },
  { path: 'layers/new', component: LayerFormComponent, canActivate: [AuthGuard] },
  { path: 'layers/edit/:id', component: LayerFormComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { faPlus, faCheckCircle, faBan } from '@fortawesome/free-solid-svg-icons';

import { NotifyService } from '../../shared/services/notify.service';
import { FirebaseService } from '../../shared/services/firebase.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit, OnDestroy {

  categories: Observable<any>;
  categoriesSub: Subscription;
  cities: Observable<any>;
  citiesSub: Subscription;
  citiesObj: any;
  showSpinner = true;
  itemToDelete: number;

  faPlus = faPlus;
  faCheckCircle = faCheckCircle;
  faBan = faBan;

  constructor(
    private firebase: FirebaseService,
    private notify: NotifyService
  ) {}

  ngOnInit() {
    this.cities = this.firebase.getCollection('/cities');
    this.citiesSub = this.cities.subscribe(data => this.citiesObj = data);

    this.categories = this.firebase.getOrderedCollection('/categories', 'order', 1);
    this.categoriesSub =  this.categories.subscribe(
      () => {
        this.showSpinner = false;
      },
      (err) => console.log(err),
      () => console.log('complete')
    );
  }

  ngOnDestroy() {
    if (this.categoriesSub) { this.categoriesSub.unsubscribe(); }
    if (this.citiesSub) { this.citiesSub.unsubscribe(); }
  }

  deleteItem(e: MouseEvent, id: string, index: number) {
    e.preventDefault();

    this.showSpinner = true;
    this.itemToDelete = index;

    const promisesArray = [];

    this.citiesObj.map(
      (city: any) => {
        const updatedPeriods = {};
        for (const j in city.periods) {
          if (j) {
            updatedPeriods[j] = {};
            for (const k in city.periods[j]) {
              if (k && k !== id) {
                updatedPeriods[j][k] = city.periods[j][k];
              }
            }

            promisesArray.push( this.firebase.updateDocument('/cities/' + city.id, { ...city, periods: updatedPeriods }) );
          }
        }

        Promise.all( promisesArray ).then(
          () => {
            // console.log('city periods updated');
            this.firebase.deleteDocument('categories', id).then(
              () => {
                this.showSpinner = false;
                this.notify.update('Great!', 'Item deleted', 'success');
              },
              (err) => console.log( err )
            );
          }
        );
      }
    );
  }
}

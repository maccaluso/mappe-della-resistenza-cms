import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';

// import { faBan, faCheck, faPlus, faUpload } from '@fortawesome/free-solid-svg-icons';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { AngularFirestoreDocument } from '@angular/fire/firestore';

import { FirebaseService } from '../../shared/services/firebase.service';
import { SlugifyService } from '../../shared/services/slugify.service';
import { NotifyService } from '../../shared/services/notify.service';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.scss']
})
export class CategoryFormComponent implements OnInit, OnDestroy {
  debug = false;

  categoryDoc: AngularFirestoreDocument<any>;
  category: Observable<any>;
  categorySub: Subscription;
  cities: Observable<any>;
  citiesSub: Subscription;
  citiesObj: any;
  periods: Observable<any>;
  periodsSub: Subscription;
  labels = {};
  isNewItem = true;
  itemSingular = 'categoria';
  itemPlural = 'categorie';
  showSpinner = true;

  categoryForm: FormGroup;
  formErrors = {
    color: '',
    order: '',
    name: '',
    slug: '',
    // labels: '',
    description: ''
  };
  validationMessages = {
    color: {
      required: 'Questo campo è obbligatorio.'
    },
    order: {
      required: 'Questo campo è obbligatorio.'
    },
    name: {
      required: 'Questo campo è obbligatorio.'
    },
    slug: {
      required: 'Questo campo è obbligatorio.'
    },
    // labels: {
    //   required: 'Questo campo è obbligatorio.'
    // }
  };

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    placeholder: 'Una breve descrizione testuale della categoria (facoltativo)...',
    translate: 'no'
  };

  // faCheck = faCheck;
  // faBan = faBan;
  // faPlus = faPlus;
  // faUpload = faUpload;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private firebase: FirebaseService,
    private slugifySvc: SlugifyService,
    private notify: NotifyService
  ) { }

  ngOnInit() {
    this.cities = this.firebase.getCollection('/cities');
    this.citiesSub = this.cities.subscribe( data => this.citiesObj = data );
    this.periods = this.firebase.getOrderedCollection('/periods', 'startDate', 1);
    this.periodsSub = this.periods.subscribe(
      (periods) => {
        periods.map(
          (period: any) => {
            this.labels[period.id] = '';
          }
        );
      }
    );

    this.categoryForm = this.fb.group({
      name: [ '', Validators.required ],
      slug: [ '', Validators.required ],
      labels: null,
      // labels: [this.fb.group({
      //   I338LWniFPWlofPCDhU9: '',
      //   l4XIQRE2Y4AvNY64irG0: '',
      //   fvBnymsP9S9Gyf95dRs9: '',
      //   YMR3coSsMfQvZbAr41GF: ''
      // }), Validators.required],
      color: [ '', Validators.required ],
      order: [ '', Validators.required ],
      description: '',
      created: '',
      lastUpdated: ''
    });

    this.categoryForm.valueChanges.subscribe((data) => this.onValueChanged(data));
    this.onValueChanged();

    this.route.params.subscribe(
      (routeParams) => {
        if (routeParams.id) {
          this.isNewItem = false;

          this.categoryDoc = this.firebase.getDocument('/categories/' + routeParams.id);
          this.category = this.categoryDoc.valueChanges();
          this.categorySub = this.category.subscribe(
            (data) => {
              this.categoryForm.setValue({
                name: data.name ? data.name : null,
                slug: data.slug ? data.slug : null,
                // labels: data.labels ? data.labels : {
                //   I338LWniFPWlofPCDhU9: '',
                //   l4XIQRE2Y4AvNY64irG0: '',
                //   fvBnymsP9S9Gyf95dRs9: '',
                //   YMR3coSsMfQvZbAr41GF: ''
                // },
                labels: data.labels ? data.labels : {},
                color: data.color ? data.color : null,
                order: data.order ? data.order : null,
                description: data.description ? data.description : null,
                created: data.created ? data.created : Date.now(),
                lastUpdated: data.lastUpdated ? data.lastUpdated : Date.now()
              });

              if (data.labels) { this.labels = data.labels; }

              this.showSpinner = false;
            },
            (err) => this.notify.update('Error!', err, 'error'),
            () => console.log('complete')
          );
        } else {
          this.showSpinner = false;
        }
      },
      (err) => this.notify.update('Error!', err, 'error'),
      () => console.log('complete')
    );
  }

  ngOnDestroy() {
    if (this.categorySub) { this.categorySub.unsubscribe(); }
    if (this.citiesSub) { this.citiesSub.unsubscribe(); }
    if (this.periodsSub) { this.periodsSub.unsubscribe(); }
  }

  onValueChanged(data?: any) {
    if (!this.categoryForm) { return; }

    const form = this.categoryForm;

    if (data) {
      form.get('slug').patchValue( this.slugifySvc.slugify( data.name ), { emitEvent: false } );
    }

    for (const field in this.formErrors) {
      if ( Object.prototype.hasOwnProperty.call( this.formErrors, field ) ) {
        // clear previous error message (if any)
        this.formErrors[field] = '';

        const control = form.get(field);

        if (control && !control.valid) {
          const messages = this.validationMessages[field];
          if (control.errors) {
            for (const key in control.errors) {
              if (Object.prototype.hasOwnProperty.call(control.errors, key) ) {
                this.formErrors[field] += `${(messages as {[key: string]: string})[key]} `;
              }
            }
          }
        }
      }
    }
  }

  save() {
    this.showSpinner = true;

    this.categoryForm.patchValue({ 
      lastUpdated: Date.now(),
      labels: this.labels
    });

    // console.log( this.categoryForm.getRawValue() );
    // return;

    if ( this.isNewItem ) {
      this.categoryForm.patchValue({ created: Date.now() });

      this.firebase.createDocument( `categories`,  this.categoryForm.getRawValue() ).then(
        (res) => {
          // console.log('category created');
          const promisesArray = [];

          for (const city of this.citiesObj) {
            for (const period in city.periods) {
              if (period) {
                city.periods[period][res.id] = {
                  enabled: false,
                  summary: 'summary: ' + period + ', ' + res.id,
                  description: '',
                  quantity: 0
                };
              }
            }

            promisesArray.push( this.firebase.updateDocument('/cities/' + city.id, city) );
            // console.log('city promise issued', city.id);
          }

          Promise.all(promisesArray).then(
            () => {
              // console.log('all city promises fullfilled');
              this.showSpinner = false;
              this.notify.update('Great!', 'Item created', 'success');
              this.router.navigate(['/categories', 'edit', res.id]);
            }
          );
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    } else {
      this.categoryDoc.update( this.categoryForm.getRawValue() ).then(
        () => {
          this.showSpinner = false;
          this.notify.update('Great!', 'Item updated', 'success');
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    }
  }

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { faPlus, faCheckCircle, faBan } from '@fortawesome/free-solid-svg-icons';

import { NotifyService } from '../../shared/services/notify.service';
import { FirebaseService } from '../../shared/services/firebase.service';

@Component({
  selector: 'app-city-list',
  templateUrl: './city-list.component.html',
  styleUrls: ['./city-list.component.scss']
})
export class CityListComponent implements OnInit, OnDestroy {

  cities: Observable<any>;
  citiesSub: Subscription;
  mapViews: Observable<any>;
  mapViewsSub: Subscription;
  showSpinner = true;
  itemToDelete: number;

  faPlus = faPlus;
  faCheckCircle = faCheckCircle;
  faBan = faBan;

  constructor(
    private firebase: FirebaseService,
    private notify: NotifyService
  ) {}

  ngOnInit() {
    this.mapViews = this.firebase.getCollection('/map-views');
    this.cities = this.firebase.getOrderedCollection('/cities', 'name', 1);
    this.citiesSub = this.cities.subscribe(
      () => {
        this.showSpinner = false;
      },
      (err) => console.log(err),
      () => console.log('complete')
    );
  }

  ngOnDestroy() {
    if (this.citiesSub) { this.citiesSub.unsubscribe(); }
    if (this.mapViewsSub) { this.mapViewsSub.unsubscribe(); }
  }

  deleteItem(e: MouseEvent, id: string, index: number) {
    e.preventDefault();

    this.showSpinner = true;
    this.itemToDelete = index;

    this.mapViewsSub = this.mapViews.subscribe(
      (mapViews: any) => {
        mapViews.map(
          (mapView: any) => {
            const updatedCities = {};
            for (const j in mapView.cities) {
              if (j && j !== id) {
                updatedCities[j] = mapView.cities[j];
              }
            }

            this.firebase.updateDocument('/map-views/' + mapView.id, { ...mapView, cities: updatedCities }).then(
              () => {
                this.firebase.deleteDocument('cities', id).then(
                  () => {
                    this.showSpinner = false;
                    this.notify.update('Great!', 'Item deleted', 'success');
                  },
                  (err) => console.log( err )
                );
              },
              (err) => console.log( err )
            );
          }
        );
      }
    );
  }

}

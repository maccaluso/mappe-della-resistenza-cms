import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { faBan, faCheck, faPlus, faEdit } from '@fortawesome/free-solid-svg-icons';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { AngularFirestoreDocument } from '@angular/fire/firestore';

import { MapBoxComponent } from '../../shared/widgets/map-box/map-box.component';

import { FirebaseService } from '../../shared/services/firebase.service';
import { SlugifyService } from '../../shared/services/slugify.service';
import { NotifyService } from '../../shared/services/notify.service';

@Component({
  selector: 'app-city-form',
  templateUrl: './city-form.component.html',
  styleUrls: ['./city-form.component.scss']
})
export class CityFormComponent implements OnInit, OnDestroy {
  @ViewChild( MapBoxComponent ) map: MapBoxComponent;
  debug = false;

  cityDoc: AngularFirestoreDocument<any>;
  city: Observable<any>;
  citySub: Subscription;
  categories: Observable<any>;
  categoriesSub: Subscription;
  periods: Observable<any>;
  periodsSub: Subscription;
  periodsData: any;
  mapViews: Observable<any>;
  mapViewsSub: Subscription;
  isNewItem = true;
  itemSingular = 'città';
  itemPlural = 'città';
  showSpinner = true;
  activeTab = 0;
  activePeriodID: string;
  firstPeriodID: string;

  cityForm: FormGroup;
  formErrors = {
    name: '',
    slug: '',
    startDate: '',
    endDate: '',
    description: ''
  };
  validationMessages = {
    startDate: {
      required: 'Questo campo è obbligatorio.'
    },
    endDate: {
      required: 'Questo campo è obbligatorio.'
    },
    name: {
      required: 'Questo campo è obbligatorio.'
    },
    slug: {
      required: 'Questo campo è obbligatorio.'
    }
  };

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    placeholder: 'Una breve descrizione testuale della città (facoltativo)...',
    translate: 'no'
  };

  faCheck = faCheck;
  faBan = faBan;
  faPlus = faPlus;
  faEdit = faEdit;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private firebase: FirebaseService,
    private slugifySvc: SlugifyService,
    private notify: NotifyService
  ) {}

  ngOnInit() {
    this.cityForm = this.fb.group({
      name: [ '', Validators.required ],
      slug: [ '', Validators.required ],
      location: null,
      description: '',
      created: '',
      lastUpdated: ''
    });

    this.periods = this.firebase.getOrderedCollection('/periods', 'startDate', 1);
    this.categories = this.firebase.getOrderedCollection('categories', 'order', 1);
    this.mapViews = this.firebase.getCollection('/map-views');
    const defaultPeriods = {};

    this.periodsSub = this.periods.subscribe(
      (periods) => {
        this.categoriesSub = this.categories.subscribe(
          (categories) => {
            periods.map(
              (period: any) => {
                defaultPeriods[period.id] = {};
                categories.map(
                  (category: any) => {
                    defaultPeriods[period.id][category.id] = {
                      enabled: false,
                      summary: 'summary: ' + period.name + ', ' + category.name,
                      description: '',
                      quantity: 0
                    };
                  }
                );
              }
            );

            if (this.isNewItem) { this.periodsData = defaultPeriods; }
            console.log(this.periodsData);
          },
          (err) => this.notify.update('Error!', err, 'error'),
          () => console.log('complete')
        );

        this.activePeriodID = periods[0].id;
        this.firstPeriodID = periods[0].id;
        // console.log( this.activePeriodID );
      },
      (err) => this.notify.update('Error!', err, 'error'),
      () => console.log('complete')
    );

    this.cityForm.valueChanges.subscribe((data) => this.onValueChanged(data));
    this.onValueChanged();

    this.route.params.subscribe(
      (routeParams) => {
        if (routeParams.id) {
          this.isNewItem = false;

          this.cityDoc = this.firebase.getDocument('/cities/' + routeParams.id);
          this.city = this.cityDoc.snapshotChanges().pipe(
            map(item => {
              const data = item.payload.data();
              const id = item.payload.id;
              return { id, ...data };
            })
          );
          this.citySub = this.city.subscribe(
            (data) => {
              this.cityForm.setValue({
                name: data.name ? data.name : null,
                slug: data.slug ? data.slug : null,
                location: data.location ? data.location : null,
                description: data.description ? data.description : null,
                created: data.created ? data.created : Date.now(),
                lastUpdated: data.lastUpdated ? data.lastUpdated : Date.now()
              });

              this.periodsData = data.periods ? data.periods : null;

              // temporary patch for category UFFICI GIUDIZIARI CENTRALI//
              for (const period in this.periodsData) {
                if (period) {
                  if (!this.periodsData[period].twKMEWdR8pl6DTdBtA5R) {
                    this.periodsData[period].twKMEWdR8pl6DTdBtA5R = {
                      enabled: false,
                      summary: '',
                      description: '',
                      quantity: 0
                    }
                  }
                }
              }
              /////////////////

              const cityGeom = {};
              if (data.location) {
                cityGeom[this.activePeriodID] = { 0: { 0: [data.location.longitude, data.location.latitude]} };
                this.map.initMap( cityGeom, this.activePeriodID );
                this.map.jump([data.location.longitude, data.location.latitude]);
              } else {
                cityGeom[this.activePeriodID] = { 0: { 0: this.map.defaultLocation} };
                this.map.initMap( cityGeom, this.activePeriodID );
              }

              this.showSpinner = false;
            },
            (err) => this.notify.update('Error!', err, 'error'),
            () => console.log('complete')
          );
        } else {
          this.showSpinner = false;
        }
      },
      (err) => this.notify.update('Error!', err, 'error'),
      () => console.log('complete')
    );
  }

  ngOnDestroy() {
    if (this.citySub) { this.citySub.unsubscribe(); }
    if (this.categoriesSub) { this.categoriesSub.unsubscribe(); }
    if (this.periodsSub) { this.periodsSub.unsubscribe(); }
    if (this.mapViewsSub) { this.mapViewsSub.unsubscribe(); }
  }

  onValueChanged(data?: any) {
    if (!this.cityForm) { return; }

    const form = this.cityForm;

    if (data) {
      form.get('slug').patchValue( this.slugifySvc.slugify( data.name ), { emitEvent: false } );
    }

    for (const field in this.formErrors) {
      if ( Object.prototype.hasOwnProperty.call( this.formErrors, field ) ) {
        this.formErrors[field] = '';

        const control = form.get(field);

        if (control && !control.valid) {
          const messages = this.validationMessages[field];
          if (control.errors) {
            for (const key in control.errors) {
              if (Object.prototype.hasOwnProperty.call(control.errors, key) ) {
                this.formErrors[field] += `${(messages as {[key: string]: string})[key]} `;
              }
            }
          }
        }
      }
    }
  }

  switchTab(e: MouseEvent, index: number, periodID: string) {
    e.preventDefault();
    this.activeTab = index;
    this.activePeriodID =  periodID;
  }

  onCategorySwitchChange(periodID: string, categoryID: string) {
    this.periodsData[periodID][categoryID].enabled = !this.periodsData[periodID][categoryID].enabled;
  }

  toggleCategoryCollapse(e: MouseEvent, periodIndex: number, catIndex: number) {
    e.preventDefault();
    document.getElementById('categoryCollapse' + periodIndex + catIndex).classList.toggle('show');
  }

  onMapLoad() { console.log('map loaded'); }

  save() {
    this.showSpinner = true;

    this.cityForm.patchValue({ lastUpdated: Date.now() });

    if (this.map.getGeometry()[this.firstPeriodID]) {
      const location = this.map.getGeometry()[this.firstPeriodID]['0']['0'];
      this.cityForm.patchValue({location: this.firebase.buildGeopoint(location[1], location[0])});
    }

    const cityObj = {
      ...this.cityForm.getRawValue(),
      periods: this.periodsData
    };

    if ( this.isNewItem ) {
      cityObj.created = Date.now();

      this.firebase.createDocument( `cities`,  cityObj ).then(
        (res) => {
          this.mapViewsSub = this.mapViews.subscribe(
            (mapViews) => {
              const promisesArray = [];
              for (const mapView of mapViews) {
                mapView.cities[res.id] = { enabled: true };
                promisesArray.push( this.firebase.updateDocument('/map-views/' + mapView.id, mapView) );
              }

              const promise = Promise.all(promisesArray);
              promise.then(
                () => {
                  this.showSpinner = false;
                  this.notify.update('Great!', 'Item created', 'success');
                  this.router.navigate(['/cities', 'edit', res.id]);
                }
              );
            }
          );
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    } else {
      this.cityDoc.update( cityObj ).then(
        () => {
          this.showSpinner = false;
          this.notify.update('Great!', 'Item updated', 'success');
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthService } from '../shared/services/auth.service';
import { FirebaseService } from '../shared/services/firebase.service';
import { saveAs } from 'file-saver';

@Component({
// tslint:disable-next-line: component-selector
  selector: 'user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss'],
})
export class UserLoginComponent implements OnInit{
  private dbCategories: Observable<any>;
  private categories: any;
  private dbCities: Observable<any>;
  private cities: any;
  private dbLayers: Observable<any>;
  private layers: any;
  private dbMapViews: Observable<any>;
  private mapViews: any;
  private dbPeriods: Observable<any>;
  private periods: any;

  private ddState = { categories: false, cities: false, layers: false, mapViews: false, periods: false };

  constructor(
    public auth: AuthService,
    private router: Router,
    public firebase: FirebaseService
  ) {
  }

  // private afterSignIn() {
  //   this.router.navigate(['/dashboard']);
  // }
  ngOnInit(){
    this.dbCategories = this.firebase.getCollection('/categories');
    this.dbCategories.subscribe(
      (data) => this.categories = data,
      (err) => console.log(err)
    );

    this.dbCities = this.firebase.getCollection('/cities');
    this.dbCities.subscribe(
      (data) => this.cities = data,
      (err) => console.log(err)
    );

    this.dbLayers = this.firebase.getCollection('/layers');
    this.dbLayers.subscribe(
      (data) => this.layers = data,
      (err) => console.log(err)
    );

    this.dbMapViews = this.firebase.getCollection('/map-views');
    this.dbMapViews.subscribe(
      (data) => this.mapViews = data,
      (err) => console.log(err)
    );

    this.dbPeriods = this.firebase.getCollection('/periods');
    this.dbPeriods.subscribe(
      (data) => this.periods = data,
      (err) => console.log(err)
    );
  }

  logOut() {
    this.auth.signOut().then(
      () => { console.log( 'Logged out' ); },
      (err) => { console.log( err ); }
    );
  }

  saveDBToFile() {
    console.log('save db');
    const data = {
      categories: this.categories,
      cities: this.cities,
      layers: this.layers,
      mapViews: this.mapViews,
      periods: this.periods
    };
    const fileName = 'mappeResistenzaDB.json';

    const fileToSave = new Blob([JSON.stringify(data, undefined, 2)], {
      type: 'application/json'
    });

    saveAs(fileToSave, fileName);
  }

}

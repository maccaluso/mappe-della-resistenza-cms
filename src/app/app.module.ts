import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { registerLocaleData } from '@angular/common';
import localeIt from '@angular/common/locales/it';
registerLocaleData(localeIt, 'it');

import { environment } from '../environments/environment';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { FirestoreSettingsToken } from '@angular/fire/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { CloudinaryModule, CloudinaryConfiguration } from '@cloudinary/angular-5.x';
import { Cloudinary } from 'cloudinary-core';

import { AppComponent } from './app.component';

import { WidgetsModule } from './shared/widgets/widgets.module';

import { FrontPageModule } from './front-page/front-page.module';
import { UserLoginModule } from './user-login/user-login.module';

import { PeriodsModule } from './periods/periods.module';
import { CategoriesModule } from './categories/categories.module';
import { CitiesModule } from './cities/cities.module';
import { MapViewModule } from './map-views/map-view.module';
import { LayerssModule } from './layers/layers.module';

import { AuthGuard } from './shared/services/auth.guard';

import { MapService } from './shared/services/map.service';

import { SlugifyService } from './shared/services/slugify.service';
import { NotifyService } from './shared/services/notify.service';
import { SymbolsService } from './shared/services/symbols.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    CloudinaryModule.forRoot({Cloudinary}, environment.cloudinary as CloudinaryConfiguration),
    WidgetsModule,
    FrontPageModule,
    UserLoginModule,
    PeriodsModule,
    CategoriesModule,
    CitiesModule,
    MapViewModule,
    LayerssModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'it-IT' },
    { provide: FirestoreSettingsToken, useValue: {} },
    AuthGuard,
    MapService,
    SlugifyService,
    NotifyService,
    SymbolsService
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }

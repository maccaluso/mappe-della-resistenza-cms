import { Component, OnInit } from '@angular/core';

import { faFileExport, faAngleDown, faSearch } from '@fortawesome/free-solid-svg-icons';
import { saveAs } from 'file-saver';

import { AuthService } from '../shared/services/auth.service';
import { FirebaseService } from '../shared/services/firebase.service';

@Component({
  selector: 'app-front-page',
  templateUrl: './front-page.component.html',
  styleUrls: ['./front-page.component.scss']
})
export class FrontPageComponent implements OnInit {
  private faFileExport = faFileExport;
  private faAngleDown = faAngleDown;
  private faSearch = faSearch;

  constructor(
    public auth: AuthService,
    public firebase: FirebaseService
  ) { }

  ngOnInit() {}

  exportDB() {
    const data = {};
    const fileName = 'mappeResistenzaDB.json';

    const fileToSave = new Blob([JSON.stringify(data, undefined, 2)], {
      type: 'application/json'
    });

    saveAs(fileToSave, fileName);
  }

}

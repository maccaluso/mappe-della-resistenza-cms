import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { faPlus, faCheckCircle, faBan } from '@fortawesome/free-solid-svg-icons';

import { NotifyService } from '../../shared/services/notify.service';
import { FirebaseService } from '../../shared/services/firebase.service';

@Component({
  selector: 'app-period-list',
  templateUrl: './period-list.component.html',
  styleUrls: ['./period-list.component.scss']
})
export class PeriodListComponent implements OnInit, OnDestroy {

  periods: Observable<any>;
  periodsSub: Subscription;
  cities: Observable<any>;
  citiesSub: Subscription;
  citiesObj: any;
  showSpinner = true;
  itemToDelete: number;

  faPlus = faPlus;
  faCheckCircle = faCheckCircle;
  faBan = faBan;

  constructor(
    private firebase: FirebaseService,
    private notify: NotifyService
  ) {}

  ngOnInit() {
    this.cities = this.firebase.getCollection('/cities');
    this.citiesSub = this.cities.subscribe(data => this.citiesObj = data);

    this.periods = this.firebase.getOrderedCollection('/periods', 'startDateTS', 1);
    this.periodsSub = this.periods.subscribe(
      () => {
        this.showSpinner = false;
      },
      (err) => console.log(err),
      () => console.log('complete')
    );
  }

  ngOnDestroy() {
    if (this.periodsSub) { this.periodsSub.unsubscribe(); }
    if (this.citiesSub) { this.citiesSub.unsubscribe(); }
  }

  deleteItem(e: MouseEvent, id: string, index: number) {
    e.preventDefault();

    this.showSpinner = true;
    this.itemToDelete = index;

    const promisesArray = [];

    this.citiesObj.map(
      (city: any) => {
        const updatedPeriods = {};
        for (const j in city.periods) {
          if (j && j !== id) {
            updatedPeriods[j] = city.periods[j];
            promisesArray.push( this.firebase.updateDocument('/cities/' + city.id, { ...city, periods: updatedPeriods }) );
          }
        }
      }
    );

    Promise.all( promisesArray ).then(
      () => {
        // console.log('city periods updated');
        this.firebase.deleteDocument('periods', id).then(
          () => {
            this.showSpinner = false;
            this.notify.update('Great!', 'Item deleted', 'success');
          },
          (err) => console.log( err )
        );
      }
    );
  }
}

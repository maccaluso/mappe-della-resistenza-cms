import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CloudinaryModule } from '@cloudinary/angular-5.x';
import { FileUploadModule } from 'ng2-file-upload';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { DpDatePickerModule } from 'ng2-date-picker';

import { WidgetsModule } from '../shared/widgets/widgets.module';

import { PeriodListComponent } from './period-list/period-list.component';
import { PeriodFormComponent } from './period-form/period-form.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    CloudinaryModule,
    FileUploadModule,
    AngularEditorModule,
    DpDatePickerModule,
    WidgetsModule
  ],
  declarations: [
    PeriodListComponent,
    PeriodFormComponent
  ]
})
export class PeriodsModule {
}

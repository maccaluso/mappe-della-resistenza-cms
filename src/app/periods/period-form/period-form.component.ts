import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';

import { faBan, faCheck, faPlus, faUpload } from '@fortawesome/free-solid-svg-icons';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { AngularFirestoreDocument } from '@angular/fire/firestore';
import * as _ from 'moment';

import { FirebaseService } from '../../shared/services/firebase.service';
import { SlugifyService } from '../../shared/services/slugify.service';
import { NotifyService } from '../../shared/services/notify.service';

@Component({
  selector: 'app-period-form',
  templateUrl: './period-form.component.html',
  styleUrls: ['./period-form.component.scss']
})
export class PeriodFormComponent implements OnInit, OnDestroy {
  debug = false;

  periodDoc: AngularFirestoreDocument<any>;
  period: Observable<any>;
  periodSub: Subscription;
  cities: Observable<any>;
  citiesSub: Subscription;
  citiesObj: any;
  categories: Observable<any>;
  categoriesSub: Subscription;
  categoriesObj: any;
  isNewItem = true;
  itemSingular = 'periodo';
  itemPlural = 'periodi';
  showSpinner = true;

  periodForm: FormGroup;
  formErrors = {
    name: '',
    slug: '',
    startDate: '',
    endDate: '',
    description: ''
  };
  validationMessages = {
    startDate: {
      required: 'Questo campo è obbligatorio.'
    },
    endDate: {
      required: 'Questo campo è obbligatorio.'
    },
    name: {
      required: 'Questo campo è obbligatorio.'
    },
    slug: {
      required: 'Questo campo è obbligatorio.'
    }
  };

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    placeholder: 'Una breve descrizione testuale della categoria (facoltativo)...',
    translate: 'no'
  };

  dpConfig: any = {
    format: 'YYYY-MM-DD',
    locale: 'it'
  };

  faCheck = faCheck;
  faBan = faBan;
  faPlus = faPlus;
  faUpload = faUpload;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private firebase: FirebaseService,
    private slugifySvc: SlugifyService,
    private notify: NotifyService
  ) { }

  ngOnInit() {
    this.cities = this.firebase.getCollection('/cities');
    this.citiesSub = this.cities.subscribe( data => this.citiesObj = data );
    this.categories = this.firebase.getCollection('/categories');
    this.categoriesSub = this.categories.subscribe( data => this.categoriesObj = data );

    this.periodForm = this.fb.group({
      name: [ '', Validators.required ],
      slug: [ '', Validators.required ],
      startDate: [ '', Validators.required ],
      startDateTS: '',
      endDate: [ '', Validators.required ],
      endDateTS: '',
      description: '',
      created: '',
      lastUpdated: ''
    });

    this.periodForm.valueChanges.subscribe((data) => this.onValueChanged(data));
    this.onValueChanged();

    this.route.params.subscribe(
      (routeParams) => {
        if (routeParams.id) {
          this.isNewItem = false;

          this.periodDoc = this.firebase.getDocument('/periods/' + routeParams.id);
          this.period = this.periodDoc.valueChanges();
          this.periodSub = this.period.subscribe(
            (data) => {
              this.periodForm.setValue({
                name: data.name ? data.name : null,
                slug: data.slug ? data.slug : null,
                startDate: data.startDate ? data.startDate : null,
                startDateTS: data.startDateTS ? data.startDateTS : null,
                endDate: data.endDate ? data.endDate : null,
                endDateTS: data.endDateTS ? data.endDateTS : null,
                description: data.description ? data.description : null,
                created: data.created ? data.created : Date.now(),
                lastUpdated: data.lastUpdated ? data.lastUpdated : Date.now()
              });

              this.showSpinner = false;
            },
            (err) => this.notify.update('Error!', err, 'error'),
            () => console.log('complete')
          );
        } else {
          this.showSpinner = false;
        }
      },
      (err) => this.notify.update('Error!', err, 'error'),
      () => console.log('complete')
    );
  }

  ngOnDestroy() {
    if (this.periodSub) { this.periodSub.unsubscribe(); }
    if (this.citiesSub) { this.citiesSub.unsubscribe(); }
    if (this.categoriesSub) { this.categoriesSub.unsubscribe(); }
  }

  onValueChanged(data?: any) {
    if (!this.periodForm) { return; }

    const form = this.periodForm;

    if (data) {
      form.get('slug').patchValue( this.slugifySvc.slugify( data.name ), { emitEvent: false } );
    }

    for (const field in this.formErrors) {
      if ( Object.prototype.hasOwnProperty.call( this.formErrors, field ) ) {
        // clear previous error message (if any)
        this.formErrors[field] = '';

        const control = form.get(field);

        if (control && !control.valid) {
          const messages = this.validationMessages[field];
          if (control.errors) {
            for (const key in control.errors) {
              if (Object.prototype.hasOwnProperty.call(control.errors, key) ) {
                this.formErrors[field] += `${(messages as {[key: string]: string})[key]} `;
              }
            }
          }
        }
      }
    }
  }

  save() {
    this.showSpinner = true;

    this.periodForm.patchValue({
      lastUpdated: _().valueOf(),
      startDateTS: _( this.periodForm.get('startDate').value ).unix(),
      endDateTS: _( this.periodForm.get('endDate').value ).unix()
    });

    if ( this.isNewItem ) {
      this.periodForm.patchValue({ created: Date.now() });

      this.firebase.createDocument( `periods`,  this.periodForm.getRawValue() ).then(
        (res) => {
          const promisesArray = [];

          for (const city of this.citiesObj) {
            city.periods[res.id] = {};

            for (const category of this.categoriesObj) {
              city.periods[res.id][category.id] = {
                enabled: false,
                summary: 'summary: ' + res.id + ', ' + category.id,
                description: '',
                quantity: 0
              };
            }

            promisesArray.push( this.firebase.updateDocument('/cities/' + city.id, city) );
          }

          Promise.all(promisesArray).then(
            () => {
              // console.log('all city promises fullfilled');
              this.showSpinner = false;
              this.notify.update('Great!', 'Item created', 'success');
              this.router.navigate(['/periods', 'edit', res.id]);
            }
          );
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    } else {
      this.periodDoc.update( this.periodForm.getRawValue() ).then(
        () => {
          this.showSpinner = false;
          this.notify.update('Great!', 'Item updated', 'success');
        },
        (err) => this.notify.update('Something went wrong!', err, 'error')
      );
    }
  }

}

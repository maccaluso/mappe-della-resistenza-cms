import { Component } from '@angular/core';

import { AuthService } from './shared/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Mappe della Resistenza CMS';
  layoutClasses = [{ sidebar: 'col-md-2', content: 'col-md-10' }];

  constructor( public auth: AuthService ) {}
}

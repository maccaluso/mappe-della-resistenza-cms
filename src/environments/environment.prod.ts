export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCm0yQ8nsbIsjbMpnme1bLM-SC_97OSq9E',
    authDomain: 'mappe-della-resistenza.firebaseapp.com',
    databaseURL: 'https://mappe-della-resistenza.firebaseio.com',
    projectId: 'mappe-della-resistenza',
    storageBucket: 'mappe-della-resistenza.appspot.com',
    messagingSenderId: '89713376711'
  },
  mapbox: {
    // accessToken: 'pk.eyJ1IjoibWFjY2FsdXNvIiwiYSI6ImNqdWFhdGdlZTAwcWszeXFxMzI2M2Z3c2cifQ.cZs9J-1MOSZDzH6oop6WeA'
    accessToken: 'pk.eyJ1IjoibWFjY2FsdXNvIiwiYSI6IjNLLVM5a0UifQ.fQiOn9nvARyZ80kya3yfjA',
    styleUrl: 'mapbox://styles/maccaluso/cju8ubgfs0plp1fp7fu3ubnmz'
  },
  cloudinary: {
    cloud_name: 'gianluca-macaluso',
    upload_preset: 'mappe-resistenza'
  }
};
